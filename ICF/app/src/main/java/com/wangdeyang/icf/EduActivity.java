package com.wangdeyang.icf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class EduActivity extends AppCompatActivity {

    List<String> lesson = new ArrayList<String>();
    private Button preButton;

    private int count = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edu);
        configureSubmitButton();
        lesson.add("Your condition may not get better or may become worse while you are in this study.");
        lesson.add("There are risks involved in taking STUDY DRUG CX-012. There may be side effects, some of which are not yet known. Problems which have not been seen before may be serious or deadly.");
        lesson.add("The most common side effects of STUDY DRUG CX-012 include:\n" +
                "•\tLow number of infection fighting white blood cells\n");

        configureNextButton();
        configurePreButton();


    }

    private void configureSubmitButton() {
        Button takeButton = findViewById(R.id.GoToQuiz);
        takeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EduActivity.this, Quiz.class));
            }

        });

    }

    private void configureNextButton() {
        Button nextButton = findViewById(R.id.NextLesson);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count< lesson.size()-1){
                    count++;
                }
//                else{
//                    Toast.makeText(EduActivity.this,
//                            "no more questions", Toast.LENGTH_LONG).show();
//                }
                EditText editText = (EditText)findViewById(R.id.Lesson);
                editText.setText(lesson.get(count), TextView.BufferType.EDITABLE);

            }

        });

    }
    private void configurePreButton() {
        preButton = findViewById(R.id.preButton);
        preButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count > 0){
                    count--;
                }
//                else{
//                    Toast.makeText(EduActivity.this,
//                            "this is the first question", Toast.LENGTH_LONG).show();
//                }
                EditText editText = (EditText)findViewById(R.id.Lesson);
                editText.setText(lesson.get(count), TextView.BufferType.EDITABLE);
            }

        });

    }
}
